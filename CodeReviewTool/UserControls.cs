﻿using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Discussion.Client;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.TeamFoundation.Build.Client;
using System.Collections;
using Excel = Microsoft.Office.Interop.Word;
using System.Configuration;

namespace CodeReviewTool
{
    public partial class UserControls : Form
    {
        private string txtfromdate;
        private string txtTodate;
        private string filepath;

        public UserControls(DateTimePicker txtfromdate, DateTimePicker txtTodate, string filepath)
        {
            InitializeComponent();
            this.txtfromdate = txtfromdate.Value.ToString("MM/dd/yyyy");
            this.txtTodate = txtTodate.Value.ToString("MM/dd/yyyy");
            this.filepath = filepath;
            loadURLs();
        }

        private void loadURLs()
        {
            for(var i=0;i<ConfigurationManager.AppSettings.Keys.Count;i++)
            {
                comboBox1.Items.Insert(i,ConfigurationManager.AppSettings.Keys[i]);
                comboBox1.SelectedIndex = 0;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var val = ConfigurationManager.AppSettings[comboBox1.SelectedItem.ToString()];
            List<CodeReviewComment> comments = new List<CodeReviewComment>();
            Uri uri = new Uri(val);
            TeamFoundationDiscussionService service = new TeamFoundationDiscussionService();
            var username = txtusername.Text;
            var pwd = txtpwd.Text;
            TfsTeamProjectCollection collection =
            new TfsTeamProjectCollection(
            uri,
            new System.Net.NetworkCredential(username, pwd));
            service.Initialize(new Microsoft.TeamFoundation.Client.TfsTeamProjectCollection(uri));
            service.Initialize(collection);
            //VersionControlServer vcs = collection.GetService<VersionControlServer>();
            //WorkItemStore wis = collection.GetService<WorkItemStore>();

            IDiscussionManager discussionManager = service.CreateDiscussionManager();

            var Store = (WorkItemStore)collection.GetService(typeof(WorkItemStore));
            var queryResult = Store.Query(String.Format("Select * from WorkItems where [Created Date] >= '{0}' AND [Created Date] <='{1}' AND [Work Item Type]='Code Review Request' AND [Team Project]='PrimeSuite'", txtfromdate, txtTodate)).Cast<WorkItem>().ToList();

            try
            {
                List<CodeReviewDetail> ListofCRD = new List<CodeReviewDetail>();
                foreach (var result in queryResult)
                {
                    
                    CodeReviewDetail CRD = new CodeReviewDetail();
                    //string frChar = result.Title.FirstOrDefault().ToString();

                    //CRD.Title = frChar + "-" + Convert.ToString(Regex.Replace(result.Title, "[^.0-9]", ""));
                    CRD.Title = result.Title;

                    CRD.WorkItemID = result.Id.ToString();
                    //CRD.AssignedTo = result.Fields.OfType<Field>().Where(x => x.Name == "Assigned To").Select(x => x.Value).FirstOrDefault().ToString();
                    CRD.CreatedBy = result.CreatedBy;
                    CRD.CreatedDate = result.CreatedDate;
                    //CRD.ReviewedBy = result.Fields.OfType<Field>().Where(x => x.Name == "Reviewed By").Select(x => x.Value).FirstOrDefault().ToString();
                    CRD.ReviewedDate = result.Fields.OfType<Field>().Where(x => x.Name == "Changed Date").Select(x => x.Value).FirstOrDefault().ToString();
                    ListofCRD.Add(CRD);
                    IAsyncResult results = discussionManager.BeginQueryByCodeReviewRequest(Convert.ToInt32(CRD.WorkItemID), QueryStoreOptions.ServerAndLocal, new AsyncCallback(CallCompletedCallback), null);
                    var output = discussionManager.EndQueryByCodeReviewRequest(results);
                    foreach (DiscussionThread thread in output)
                    {
                        if (thread.RootComment != null)
                        {
                            if (CRD.ModifiedBy == null)
                            {
                                CRD.ModifiedBy = new List<string> { thread.RootComment.Author.DisplayName };
                            }
                            else
                            {
                                CRD.ModifiedBy.Add(thread.RootComment.Author.DisplayName);
                            }
                            if (CRD.childcomments == null)
                            {
                                var cmts = thread.RootComment.Content.Split(':');
                                if (cmts.Count() > 1)
                                {
                                    CRD.Category = new List<string> { cmts[0] };
                                    CRD.childcomments = new List<string> { cmts[1] };
                                }
                                else
                                {
                                    CRD.Category = new List<string> { String.Empty };
                                    CRD.childcomments = new List<string> { cmts[0] };
                                }
                            }
                            else
                            {
                                var cmts = thread.RootComment.Content.Split(':');
                                if (cmts.Count() > 1)
                                {
                                    CRD.Category.Add(cmts[0]);
                                    CRD.childcomments.Add(cmts[1]);
                                }
                                else
                                {
                                    CRD.Category.Add(String.Empty);
                                    CRD.childcomments.Add(cmts[0]);
                                }

                            }
                            if (CRD.filepath == null)
                            {
                                CRD.filepath = new List<string> { thread.ItemPath == null ? "OverAll Comment" : thread.ItemPath };
                            }
                            else
                            {
                                CRD.filepath.Add(thread.ItemPath == null ? "OverAll Comment" : thread.ItemPath);
                            }


                        }

                    }


                }
                WriteToExcel(ListofCRD);
                MessageBox.Show("Report Generation completed..!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        static void CallCompletedCallback(IAsyncResult result)
        {
            // Handle error conditions here
        }

        public void WriteToExcel(List<CodeReviewDetail> rrr)
        {

            try
            {
                if (filepath != string.Empty)
                {
                    StreamWriter streamWriter = null;
                    if (streamWriter == null)
                    {
                        streamWriter = new StreamWriter(filepath + @"\" + "TfsReport.xls", false);
                    }

                    List<string> list = new List<string>();
                    list.Add("Defect Id");
                    list.Add("File Path");
                    //list.Add("Assigned To");
                    list.Add("Created By");
                    list.Add("Created Date");
                    list.Add("Modified By");
                    list.Add("Reviewed Date");
                    list.Add("Category");
                    list.Add("Comments");
                    for (int i = 0; i < list.Count; i++)
                    {
                        streamWriter.Write(list[i] + "\t");
                    }


                    streamWriter.WriteLine();
                    foreach (var result in rrr)
                    {
                        if (result != null)
                        {
                            if (result.childcomments != null)
                            {
                                for (var i = 0; i < result.childcomments.Count; i++)
                                {
                                    if (result.childcomments.Count > 0)
                                    {
                                        streamWriter.Write(result.Title + "\t" + result.filepath[i] + "\t"
                                        //+ result.AssignedTo + "\t"
                                        + result.CreatedBy + "\t" + Convert.ToDateTime(result.CreatedDate).ToString("MM/dd/yyyy") + "\t" +
                                        result.ModifiedBy[i] + "\t" +
                                         Convert.ToDateTime(result.ReviewedDate).ToString("MM/dd/yyyy") + "\t" + result.Category[i] + "\t" + result.childcomments[i]);
                                        streamWriter.WriteLine();
                                    }
                                }
                            }
                        }
                        

                    }
                    streamWriter.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



    }
    public class CodeReviewComment
    {
        public string Author { get; set; }
        public string Comment { get; set; }
        public string PublishDate { get; set; }
        public string ItemName { get; set; }
    }

}