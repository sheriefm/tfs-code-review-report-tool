﻿using System;
using System.Windows.Forms;

namespace CodeReviewTool
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                UserInterface ui = new UserInterface();
                ui.ShowDialog();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}

