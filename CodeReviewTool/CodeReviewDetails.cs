﻿using Microsoft.TeamFoundation.Discussion.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeReviewTool
{
    
    public class  CodeReviewDetail
    {
                
        public string Title   { get; set; }
        public string WorkItemID { get; set; }
        //public string AssignedTo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<String> ModifiedBy { get; set; }
        public string ReviewedDate { get; set; }
        public List<String> Category { get; set; }
        public List<String> childcomments { get; set; }
        public List<String> filepath { get; set; }
        //public string content { get; set; }


    }
    
}
